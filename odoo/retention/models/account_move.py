"""
This is a python module used to modify the account_move module
"""

import logging
from odoo import models, fields, api

_logger = logging.getLogger(__name__)


class AccountMove(models.Model):
    """
    This model is override to include, functionalities related to retentions module.
    """
    _name = "account.move"
    _inherit = "account.move"

    retention_total = fields.Monetary(string='Retencion', readonly=True,compute='_compute_retention')

    @api.depends("amount_total")
    def _applies_for_retention(self):
        _logger.error(self.partner_id.retention_id.applies_retention)
