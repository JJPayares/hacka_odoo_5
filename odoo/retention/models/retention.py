"""
This is a python module built to implement the retention odoo module
"""

import logging
from odoo import api, models, fields, models

_logger = logging.getLogger(__name__)


class Retention(models.Model):
    """
    Model to manage the user's retentions
    """
    _name = "retention.retention"
    _description = "Manage the user retentions."
    _rec_name = "id"
    _order = "create_date"

    user_id = fields.Many2one('res.partner', string='user retention', store=True, readonly=True)
    applies_retention = fields.Boolean(default=False, string='Applies for retention?')
    retention_percent = fields.Integer(string="Retention percent.")
    retention_top = fields.Float(string="Retention amount top.")
