"""
This is a python module used to modify the res_partner module
"""
from odoo import models, fields


class ResClient(models.Model):
    """
    Model used to override the default res_partner model.
    """
    _name = "res.partner"
    _inherit = 'res.partner'

    retention_id = fields.One2many('retention.retention', inverse_name="user_id", string="retention")
