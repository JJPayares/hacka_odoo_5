{
    "name": 'Retentions',
    "author": 'Alan Sanchez, Erick Contreras, Juan Payares',
    "version": '1.0.0-dev07',
    "summary": 'This module implements retention discount to invoices',
    "depends": ['account',],
    "data": [
        'security/user_groups.xml',
        'security/ir.model.access.csv',
        'views/retention_views.xml',
        'views/res_partner_views.xml',
    ],
}
